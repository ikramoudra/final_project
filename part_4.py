import pandas as pd
import os
BASE_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__)))
import random
import datetime as dt
import warnings
warnings.filterwarnings('ignore')
import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    """ create a database connection to a SQLite database """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        print(sqlite3.version)
        return conn
    except Error as e:
        print(e)
    finally:
        if conn:
            conn.close()


def create_table(dbfile):
    """
    Creates a table called simulation -> save the simulations data
    generated with Customer Class
    :param dbfile:
    :return: None
    """
    conn = sqlite3.connect(dbfile)
    conn.execute("""CREATE TABLE simulation (
                    TIME text,
                    CUSTOMERID text,
                    CUSTOMERTYPE text,
                    COURSE1 text,
                    COURSE2 text,
                    COURSE3 text,
                    DRINKS1 REAL,
                    DRINKS2 REAL,
                    DRINKS3 REAL,
                    TOTAL1 REAL,
                    TOTAL2 REAL,
                    TOTAL3 REAL,
                    DATE text
                    );""")



# conn = sqlite3.connect(dbfile)
# create_table(os.path.join(BASE_PATH, 'db', 'database.db'))

def insert_table(df, dbfile):

    tuples = list(df.itertuples(index=False, name=None))
    columns_list = df.columns.tolist()
    marks = ['?' for _ in columns_list]
    columns_list = f'({(",".join(columns_list))})'
    marks = f'({(",".join(marks))})'
    table_name = 'simulation'
    conn = sqlite3.connect(dbfile)
    conn.executemany(f'INSERT OR REPLACE INTO {table_name}{columns_list} VALUES {marks}', tuples)
    conn.commit()


class Customer:
    """
    Customer class
    Here we generate all the distributions (type of customer, dish, drink costs)
    This class will be called to generate each restaurant event
    """

    def __init__(self, raw_df):
        self.raw_df = raw_df
        self.fix_data_frame()
        self.client_types = self.raw_df['label_comparison'].unique()
        self.load_dists_food()
        self.load_dists_drink()
        self.defineClientType()
        self.load_dist_time()
        self.defineClientID()
        self.goToRestaurant()
        self.defineDataFrame()

    def fix_data_frame(self):
        """
        Fixing DataFrame
        :return: None -> However changes the self.raw_df
        """

        self.raw_df = self.raw_df.loc[:, ~self.raw_df.columns.str.endswith('_y')]
        for col in self.raw_df.columns:
            if '_x' in col:
                self.raw_df.rename(columns={col: col.split('_x')[0]}, inplace=True)
            elif 'Unnamed' in col:
                self.raw_df.drop(col, inplace=True, axis=1)


    def defineDataFrame(self):
        """
        Generates the final output of the simulation
        dataframe with one row and all the information needed to include on the database
        """

        prices = self.menu_prices()

        final_df = pd.DataFrame({
                                 'TIME': self.time,
                                 'CUSTOMERID': [self.client_id],
                                 'CUSTOMERTYPE': self.client_type,
                                 'COURSE1': self.dish_1,
                                 'COURSE2': self.dish_2,
                                 'COURSE3': self.dish_3,
                                 'DRINKS1': self.drink_1,
                                 'DRINKS2': self.drink_2,
                                 'DRINKS3': self.drink_3,
                                 'TOTAL1': prices[self.dish_1] + self.drink_1,
                                 'TOTAL2': prices[self.dish_2] + self.drink_2,
                                 'TOTAL3': prices[self.dish_3] + self.drink_3
                                    })

        self.final_df = final_df

    def goToRestaurant(self):
        """
        Go to restaurant event
        """

        self.dish_1 = random.choices(self.info_dist[self.client_type[0]]['dish_1']['Dish_1Course'],
                                     self.info_dist[self.client_type[0]]['dish_1']['percentage'])[0]

        self.dish_2 = random.choices(self.info_dist[self.client_type[0]]['dish_2']['Dish_2Course'],
                                     self.info_dist[self.client_type[0]]['dish_2']['percentage'])[0]

        self.dish_3 = random.choices(self.info_dist[self.client_type[0]]['dish_3']['Dish_3Course'],
                                     self.info_dist[self.client_type[0]]['dish_3']['percentage'])[0]

        self.drink_1_prob = random.choices(self.info_dist_drinks[self.client_type[0]]['drink_1']['course_1_drink'],
                                           self.info_dist_drinks[self.client_type[0]]['drink_1']['percentage'])[0]

        self.drink_2_prob = random.choices(self.info_dist_drinks[self.client_type[0]]['drink_2']['course_2_drink'],
                                           self.info_dist_drinks[self.client_type[0]]['drink_2']['percentage'])[0]

        self.drink_3_prob = random.choices(self.info_dist_drinks[self.client_type[0]]['drink_3']['course_3_drink'],
                                           self.info_dist_drinks[self.client_type[0]]['drink_3']['percentage'])[0]

        self.time = random.choices(self.time_dist_prob[self.client_type[0]]['TIME'].values,
                                    self.time_dist_prob[self.client_type[0]]['percentage'].values)[0]

        self.define_drink_price_starter()

        self.define_drink_price_main()

        self.define_drink_price_dessert()

    def define_drink_price_starter(self):
        """
        Method aiming to define the price of the drink on the starter
        """
        if self.dish_1 == 'Soup':
            self.drink_1 = self.drink_1_prob * float(random.randrange(0, 8))

        elif self.dish_1 == 'Tomate Mozarella':
            self.drink_1 = self.drink_1_prob * float(random.randrange(0, 5))

        elif self.dish_1 == 'Oyster':
            self.drink_1 = self.drink_1_prob * float(random.randrange(0, 10))

        else:
            self.drink_1 = 0


    def define_drink_price_main(self):
        """
        Method aiming to define the price of the drink following the main course
        """
        if self.dish_2 == 'Salad':
            self.drink_2 = self.drink_2_prob * random.randrange(0, 11)

        elif self.dish_2 == 'Spaghetti':
            self.drink_2 = self.drink_2_prob * random.randrange(0, 5)

        elif self.dish_2 == 'Steak':
            self.drink_2 = self.drink_2_prob * random.randrange(0, 15)

        elif self.dish_2 == 'Lobster':
            self.drink_2 = self.drink_2_prob * random.randrange(0, 15)

        else:
            self.drink_2 = 0

    def define_drink_price_dessert(self):
        """
        Method aiming to define the price of the drink following the dessert
        """

        if self.dish_3 == 'Ice Cream':
            self.drink_3 = self.drink_3_prob * random.randrange(0, 5)

        elif self.dish_3 == 'Pie':
            self.drink_3 = self.drink_2_prob * random.randrange(0, 5)

        else:
            self.drink_3 = 0



    @staticmethod
    def _returning_client_probs():
        """
        :return: The clients' probability of returning to the restaurant
        """
        probability_ret = {'Business': 0.5,
                           'Healthy': 0.7,
                           'Retirement': 0.9,
                           'Onetime': 0.0
                           }
        return probability_ret


    @staticmethod
    def menu_prices():
        """
        :return: Menu prices in a dict
        """
        d = {'Soup':  3,
             'Tomate Mozarella': 15,
             'Oyster': 20,
             'Salad': 9,
             'Spaghetti': 20,
             'Steak': 25,
             'Lobster': 40,
             'Ice cream': 15,
             'Pie': 10
        }

        return d

    def defineClientID(self):
        """
        Definies ClientID depeding if they are coming back or
        if they are new customers

        """
        df = self.raw_df[['CLIENT_ID', 'label_comparison']]

        ret_prob = self._returning_client_probs()

        self.info_returning_client = {}

        for key in ret_prob:
            x = pd.DataFrame({'info': ['returning', 'no_returning'], 'prob': [ret_prob[key],
                                                                                        1-ret_prob[key]]})

            self.info_returning_client[key] = x

        returning_flag = random.choices(self.info_returning_client[self.client_type[0]]['info'].values,
                                    self.info_returning_client[self.client_type[0]]['prob'].values)[0]


        if returning_flag == 'no_returning':
            conn = sqlite3.connect(os.path.join('db', 'database.db'))

            max_id = pd.read_sql_query("SELECT max(CUSTOMERID) as max FROM simulation", conn)['max'].values[0]

            if max_id == None:
                self.client_id = 'ID1'
            else:
                self.client_id = 'ID' + str(int(max_id.strip('ID')) + 1)
        else:
            get_id = random.randint(0, len(df[df['label_comparison'] == self.client_type[0]]))
            self.client_id = list(df[df['label_comparison'] == self.client_type[0]]['CLIENT_ID'])[get_id-1]



    def defineClientType(self):
        """
        Defines client type distribution
        """

        self.client_type = random.choices(self.total_dist['label_comparison'], self.total_dist['percentage'])



    def load_dist_time(self):
        """
        Defines the distribution time of each type of client

        """

        df = self.raw_df[['TIME', 'label_comparison']]

        df['count'] = 1

        self.time_dist = df.groupby(['TIME', 'label_comparison']).agg({'count': sum}).reset_index()

        self.time_dist_prob = {}

        for type in  self.time_dist['label_comparison'].unique():
            x = self.time_dist.loc[self.time_dist['label_comparison']==type]
            x['percentage'] = x['count']/x['count'].sum()
            self.time_dist_prob[type] = x


    def load_dists_food(self):
        """
        Defines the food distribution for each client type
        """

        df = self.raw_df[['Dish_1Course', 'Dish_2Course', 'Dish_3Course', 'label_comparison']]

        self.info_dist = {}

        df['count'] = 1

        melted_df = pd.melt(df, id_vars=['label_comparison', 'Dish_1Course',
                                         'Dish_2Course', 'Dish_3Course'], value_vars=['count'])

        self.total_dist = melted_df.groupby('label_comparison').agg({'value': sum}).reset_index()
        self.total_dist['percentage'] = self.total_dist['value'] / self.total_dist['value'].sum()

        for t in self.client_types:
            x = melted_df[melted_df['label_comparison'] == t]
            melted_df_dish_1 = x.groupby('Dish_1Course').agg({'value': sum}).reset_index()
            melted_df_dish_1['percentage'] = melted_df_dish_1['value'] / melted_df_dish_1['value'].sum()
            melted_df_dish_2 = melted_df.groupby('Dish_2Course').agg({'value': sum}).reset_index()
            melted_df_dish_2['percentage'] = melted_df_dish_2['value'] / melted_df_dish_2['value'].sum()
            melted_df_dish_3 = melted_df.groupby('Dish_3Course').agg({'value': sum}).reset_index()
            melted_df_dish_3['percentage'] = melted_df_dish_3['value'] / melted_df_dish_3['value'].sum()

            self.info_dist[t] = {
                'dish_1': melted_df_dish_1,
                'dish_2': melted_df_dish_2,
                'dish_3': melted_df_dish_3,
            }

    def load_dists_drink(self):
        """
        Defining the drink distribution for each client type
        """
        df = self.raw_df[[ 'course_1_drink', 'course_2_drink', 'course_3_drink', 'label_comparison']]
        self.info_dist_drinks = {}

        df['count'] = 1

        melted_df = pd.melt(df, id_vars=['label_comparison', 'course_1_drink',
                                         'course_2_drink', 'course_3_drink'], value_vars=['count'])

        self.total_dist = melted_df.groupby('label_comparison').agg({'value': sum}).reset_index()
        self.total_dist['percentage'] = self.total_dist['value'] / self.total_dist['value'].sum()

        for t in self.client_types:
            x = melted_df[melted_df['label_comparison'] == t]
            melted_df_dish_1 = x.groupby('course_1_drink').agg({'value': sum}).reset_index()
            melted_df_dish_1['percentage'] = melted_df_dish_1['value'] / melted_df_dish_1['value'].sum()
            if len(melted_df_dish_1.loc[melted_df_dish_1['course_1_drink'] == 0]['percentage']) > 0:
                percentage_1_drink = melted_df_dish_1.loc[melted_df_dish_1['course_1_drink'] == 0]['percentage'].values[
                    0]
            else:
                percentage_1_drink = 1

            melted_df_dish_2 = melted_df.groupby('course_2_drink').agg({'value': sum}).reset_index()
            melted_df_dish_2['percentage'] = melted_df_dish_2['value'] / melted_df_dish_2['value'].sum()

            if len(melted_df_dish_2.loc[melted_df_dish_2['course_2_drink'] == 0]['percentage']) > 0:
                percentage_2_drink = melted_df_dish_2.loc[melted_df_dish_2['course_2_drink'] == 0]['percentage'].values[
                    0]
            else:
                percentage_2_drink = 1

            melted_df_dish_3 = melted_df.groupby('course_3_drink').agg({'value': sum}).reset_index()
            melted_df_dish_3['percentage'] = melted_df_dish_3['value'] / melted_df_dish_3['value'].sum()

            if len(melted_df_dish_3.loc[melted_df_dish_3['course_3_drink'] == 0]['percentage']) > 0:
                percentage_3_drink = melted_df_dish_3.loc[melted_df_dish_3['course_3_drink'] == 0]['percentage'].values[
                    0]
            else:
                percentage_3_drink = 1

            self.info_dist_drinks[t] = {
                'drink_1': pd.DataFrame(
                    {'course_1_drink': [0, 1], 'percentage': [percentage_1_drink, 1 - percentage_1_drink]}),
                'drink_2': pd.DataFrame(
                    {'course_2_drink': [0, 1], 'percentage': [percentage_2_drink, 1 - percentage_2_drink]}),
                'drink_3': pd.DataFrame(
                    {'course_3_drink': [0, 1], 'percentage': [percentage_3_drink, 1 - percentage_3_drink]}),
            }




if __name__ == '__main__':

    conn = sqlite3.connect(os.path.join(BASE_PATH, 'db', 'database.db'))

    last_day = pd.read_sql_query("SELECT max(DATE) as last_date FROM simulation", conn)['last_date'].values[0]

    df_today = pd.read_sql_query(f"SELECT * FROM simulation WHERE DATE = '{last_day}'", conn)

    today = pd.read_sql_query("SELECT max(DATE) as last_date FROM simulation", conn)['last_date'].values[0]

    init_date = dt.datetime.strptime(today, '%Y-%m-%d') + dt.timedelta(1)
    end_date = dt.datetime.strptime(today, '%Y-%m-%d') + dt.timedelta(2)

    range_dates = pd.date_range(init_date, end_date)

    raw_df = pd.read_excel(os.path.join('Results', 'Part3', 'Courses_With_Label.xlsx'))

    for day in range_dates:

        for i in range(20):

            print(i, day)

            x = Customer(raw_df)

            result = x.final_df

            result['DATE'] = day.strftime("%Y-%m-%d")

            insert_table(result, os.path.join(BASE_PATH, 'db', 'database.db'))
