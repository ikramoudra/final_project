from Model import model
import pandas as pd
from Reader import reader
import os
import matplotlib.pyplot as plt
BASE_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__)))
path = os.path.join(BASE_PATH, 'Data', 'part3.csv') #endroit du part1
dataframe = reader.read_csv(path) #lire le fichier csv
import numpy as np
from part_2 import y

map_labels={'Fitness customer': 'Healthy',
 'Retired customer':'Retirement',
 'Business customer':'Business',
 'Other customers':'Onetime'
 } #Create a dictionnary in order to map the labels

merged_dataframe=pd.merge(dataframe, y, how='left',on='CLIENT_ID') #merge  dataframe

merged_dataframe['label_comparison'] = merged_dataframe['label'].apply(lambda x : map_labels[x]) #Changing our labels with the actual labels

merged_dataframe['Comparison'] = merged_dataframe['label_comparison'] == merged_dataframe['CLIENT_TYPE'] # Create a column 'Comparison' between the two labels with a true or false output

True_Values = len(merged_dataframe[merged_dataframe['Comparison'] == True])
False_Values = len(merged_dataframe[merged_dataframe['Comparison'] == False])

merged_dataframe.to_excel(os.path.join(BASE_PATH, 'Results','Part3', 'Comparison.xlsx'))

Accuracy = True_Values/(True_Values+False_Values)

print(f'Accuracy of :{Accuracy}')

def distribution(dataframe):
    dataframe['Count']=1 #create a column of the count
    grouped=dataframe.groupby('label').agg({'Count':sum}).reset_index()  #group the labels to count each type of customer
    grouped['Percentage']=grouped['Count']/grouped['Count'].sum() #Create a percentage column of each type of customer
    labels=grouped['label']
    sizes = grouped['Percentage']

    fig1, ax1 = plt.subplots()
    ax1.pie(sizes, labels=labels, autopct='%1.1f%%',
            shadow=True, startangle=90)
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

    plt.show()
    plt.savefig(os.path.join(BASE_PATH, 'Results', 'Part3', 'Distribution of customers.jpg'))
    return grouped

def likelihood(dataframe):

    def value_(x): #Define function to attribute 0 or 1 depending if the customer had the crouse
        if x == 0:
            return 0
        else:
            return 1

    dataframe['First_course_count'] = dataframe['FIRST_COURSE'].apply(
        lambda x: 0 if x==0 else 1)  # Create a new column based on the course price , lambda is for apply the value function
    dataframe['First_course_count']=dataframe['FIRST_COURSE'].apply(lambda x: value_(x)) #Create a new column based on the course price , lambda is for apply the value function
    dataframe['Second_course_count']=dataframe['SECOND_COURSE'].apply(lambda x: value_(x))
    dataframe['Third_course_count']=dataframe['THIRD_COURSE'].apply(lambda x: value_(x))
    grouped = dataframe.groupby('label').agg(  {'First_course_count': sum,'Second_course_count':sum,'Third_course_count':sum}).reset_index()  # group the labels to count each type of customer
    grouped['Sum_Courses_per_client'] = grouped[['First_course_count', 'Second_course_count', 'Third_course_count']].sum(axis=1) #Sum the total course for each type of client
    grouped['Percentage_First_course']=grouped['First_course_count']/grouped['Sum_Courses_per_client'] #Create a percentage column of each type of customer
    grouped['Percentage_Second_course']=grouped['Second_course_count']/grouped['Sum_Courses_per_client'] #Create a percentage column of each type of customer
    grouped['Percentage_Third_course']=grouped['Third_course_count']/grouped['Sum_Courses_per_client'] #Create a percentage column of each type of customer


    for l in grouped['label'].unique():
        x = grouped.loc[grouped['label'] == l]
        x = x.filter(items=['Percentage_First_course', 'Percentage_Second_course', 'Percentage_Third_course'])
        labels = ['FIRST_COURSE', 'SECOND_COURSE', 'THIRD_COURSE']
        sizes = np.array(x[['Percentage_First_course', 'Percentage_Second_course', 'Percentage_Third_course']])[0]
        fig1, ax1 = plt.subplots()
        ax1.pie(sizes, labels=labels, autopct='%1.1f%%',
                shadow=True, startangle=90)
        ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
        ax1.set_title(l)
        plt.savefig(os.path.join(BASE_PATH, 'Results', 'Part3', 'Likelihood', f'Likelihood {l}.jpg'))
        plt.show()

    return grouped



def dish_1course(price) -> str: #classify the dishes by its price

    if price==3:
        dish = 'Soup'
    elif price == 15:
        dish = 'Tomate Mozarella'
    elif price ==20:
        dish = 'Oyster'
    else:
        dish=None
    return dish
def dish_2course(price) -> str:
    if price==9:
        dish = 'Salad'
    elif price == 20:
        dish = 'Spaghetti'
    elif price ==25:
        dish = 'Steak'
    elif price==40:
        dish = 'Lobster'
    else:
        dish = None
    return dish

def dish_3course(price) -> str:

    if price==15:
        dish = 'Ice cream'
    elif price == 10:
        dish = 'Pie'
    else:
        dish = None
    return dish

x = distribution(merged_dataframe)
y = likelihood(merged_dataframe)


dish_prices_dataframe = pd.read_excel(os.path.join(BASE_PATH, 'Results', 'Part1', 'Cost of drinks.xlsx'))

dish_prices_dataframe['Dish_1Course'] = dish_prices_dataframe['course_1_food'].apply(lambda x: dish_1course(x)) #j'applique la fonction dans chaque ligne de la colonne
dish_prices_dataframe['Dish_2Course'] = dish_prices_dataframe['course_2_food'].apply(lambda x: dish_2course(x)) #j'applique la fonction dans chaque ligne de la colonne
dish_prices_dataframe['Dish_3Course'] = dish_prices_dataframe['course_3_food'].apply(lambda x: dish_3course(x)) #j'applique la fonction dans chaque ligne de la colonne

# merge w/ customer type dataframe

merged_dish_prices_dataframe = pd.merge(merged_dataframe, dish_prices_dataframe, how='left',on='CLIENT_ID')

merged_dish_prices_dataframe.to_excel(os.path.join(BASE_PATH, 'Results', 'Part3', 'Courses_With_Label.xlsx'))

# add a column named count
merged_dish_prices_dataframe['Dish_1course_count'] = 1  #Create a column value=1
merged_dish_prices_dataframe['Dish_2course_count'] = 1
merged_dish_prices_dataframe['Dish_3course_count'] = 1
grouped_dish_1 = merged_dish_prices_dataframe.groupby(['label', 'Dish_1Course']).agg({'Dish_1course_count': sum}).reset_index()
grouped_dish_2 = merged_dish_prices_dataframe.groupby(['label', 'Dish_2Course']).agg({'Dish_2course_count': sum}).reset_index()
grouped_dish_3 = merged_dish_prices_dataframe.groupby(['label', 'Dish_3Course']).agg({'Dish_3course_count': sum}).reset_index()


agg_dish_1_business = grouped_dish_1[grouped_dish_1['label']=='Business customer'].groupby('Dish_1Course').agg({'Dish_1course_count':sum}).reset_index() #group eqch client type with each dish course
agg_dish_2_business = grouped_dish_2[grouped_dish_2['label']=='Business customer'].groupby('Dish_2Course').agg({'Dish_2course_count':sum}).reset_index()
agg_dish_3_business = grouped_dish_3[grouped_dish_3['label']=='Business customer'].groupby('Dish_3Course').agg({'Dish_3course_count':sum}).reset_index()

agg_dish_1_other = grouped_dish_1[grouped_dish_1['label'] == 'Other customers'].groupby('Dish_1Course').agg({'Dish_1course_count':sum}).reset_index()
agg_dish_2_other = grouped_dish_2[grouped_dish_2['label'] == 'Other customers'].groupby('Dish_2Course').agg({'Dish_2course_count':sum}).reset_index()
agg_dish_3_other = grouped_dish_3[grouped_dish_3['label'] == 'Other customers'].groupby('Dish_3Course').agg({'Dish_3course_count':sum}).reset_index()

agg_dish_1_retired = grouped_dish_1[grouped_dish_1['label']=='Retired customer'].groupby('Dish_1Course').agg({'Dish_1course_count':sum}).reset_index()
agg_dish_2_retired = grouped_dish_2[grouped_dish_2['label']=='Retired customer'].groupby('Dish_2Course').agg({'Dish_2course_count':sum}).reset_index()
agg_dish_3_retired = grouped_dish_3[grouped_dish_3['label']=='Retired customer'].groupby('Dish_3Course').agg({'Dish_3course_count':sum}).reset_index()


agg_dish_1_fitness = grouped_dish_1[grouped_dish_1['label'] == 'Fitness customer'].groupby('Dish_1Course').agg({'Dish_1course_count':sum}).reset_index()
agg_dish_2_fitness = grouped_dish_2[grouped_dish_2['label'] == 'Fitness customer'].groupby('Dish_2Course').agg({'Dish_2course_count':sum}).reset_index()
agg_dish_3_fitness = grouped_dish_3[grouped_dish_3['label']=='Fitness customer'].groupby('Dish_3Course').agg({'Dish_3course_count':sum}).reset_index()

agg_dish_1_business['Percentage_Dish']=agg_dish_1_business['Dish_1course_count']/agg_dish_1_business['Dish_1course_count'].sum()
agg_dish_2_business['Percentage_Dish']=agg_dish_2_business['Dish_2course_count']/agg_dish_2_business['Dish_2course_count'].sum()
agg_dish_3_business['Percentage_Dish']=agg_dish_3_business['Dish_3course_count']/agg_dish_3_business['Dish_3course_count'].sum()
agg_dish_1_other['Percentage_Dish']=agg_dish_1_other['Dish_1course_count']/agg_dish_1_other['Dish_1course_count'].sum()
agg_dish_2_other['Percentage_Dish']=agg_dish_2_other['Dish_2course_count']/agg_dish_2_other['Dish_2course_count'].sum()
agg_dish_3_other['Percentage_Dish']=agg_dish_3_other['Dish_3course_count']/agg_dish_3_other['Dish_3course_count'].sum()
agg_dish_1_retired['Percentage_Dish']=agg_dish_1_retired['Dish_1course_count']/agg_dish_1_retired['Dish_1course_count'].sum()
agg_dish_2_retired['Percentage_Dish']=agg_dish_2_retired['Dish_2course_count']/agg_dish_2_retired['Dish_2course_count'].sum()
agg_dish_3_retired['Percentage_Dish']=agg_dish_3_retired['Dish_3course_count']/agg_dish_3_retired['Dish_3course_count'].sum()
agg_dish_1_fitness['Percentage_Dish']=agg_dish_1_fitness['Dish_1course_count']/agg_dish_1_fitness['Dish_1course_count'].sum()
agg_dish_2_fitness['Percentage_Dish']=agg_dish_2_fitness['Dish_2course_count']/agg_dish_2_fitness['Dish_2course_count'].sum()
agg_dish_3_fitness['Percentage_Dish']=agg_dish_3_fitness['Dish_3course_count']/agg_dish_3_fitness['Dish_3course_count'].sum()

info_dist_dishes = {

    'business_1st_course': agg_dish_1_business,
    'business_2nd_course': agg_dish_2_business,
    'business_3rd_course': agg_dish_3_business,
    'other_1st_course': agg_dish_1_other,
    'other_2nd_course': agg_dish_2_other,
    'other_3rd_course': agg_dish_3_other,
    'retired_1st_course': agg_dish_1_retired,
    'retired_2nd_course': agg_dish_2_retired,
    'retired_3rd_course': agg_dish_3_retired,
    'fitness_1st_course': agg_dish_1_fitness,
    'fitness_2nd_course': agg_dish_2_fitness,
    'fitness_3rd_course': agg_dish_3_fitness,
}


for key in info_dist_dishes.keys():
    info_dist_dishes[key].columns = ['dish', 'count', 'percentage']

    labels = info_dist_dishes[key]['dish']
    sizes = np.array(info_dist_dishes[key]['percentage'])
    fig1, ax1 = plt.subplots()
    ax1.pie(sizes, labels=labels, autopct='%1.1f%%',
            shadow=True, startangle=90)
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    ax1.set_title(key)
    plt.savefig(os.path.join(BASE_PATH, 'Results', 'Part3','Dishes_Dist', f'Dishes Distribution {key}.jpg'))
    plt.show()


drinks_group_first_course = dish_prices_dataframe[['course_1_drink']]
drinks_group_second_course = dish_prices_dataframe[['course_2_drink']]
drinks_group_third_course = dish_prices_dataframe[['course_3_drink']]

list_drinks = [drinks_group_first_course, drinks_group_second_course, drinks_group_third_course]


for l in list_drinks:
    counts, bins = np.histogram(l)
    plt.figure()
    plt.hist(bins[:-1], 50, weights=counts)
    plt.title(l.columns[0])
    plt.show()
    plt.savefig(os.path.join(BASE_PATH, 'Results', 'Part3', 'Drinks_Dist', f'{l.columns[0]}.jpg'))


