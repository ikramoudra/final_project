import matplotlib.pyplot as plt
from Reader import reader

import numpy as np
import os
BASE_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__))) #Avoir le chemin du projet


def plotting_hist(dataframe: object):  # fonction de plotting du dataframe resultat qu'on a eu apres avoir lu le fichier csv

    fig, axs = plt.subplots(1, 3, figsize=(15, 5)) #choisir le nbr des columns et lignes de la figure et taille (1 ligne , 3 colomns , 15x,5y)
    fig.suptitle('Histogram - Courses') #title de lq figure
    for col in dataframe.columns: #pour chaque colomne du dataframe
        if col == 'FIRST_COURSE':
            axs[0].hist(dataframe[col], color='blue') #axe 0 un histo qvec dataframe first course
            axs[0].set_title('First Course')     # Titre de la figure 1
        if col =='SECOND_COURSE':
            axs[1].hist(dataframe[col], color='green')
            axs[1].set_title('Second Course')
        if col =='THIRD_COURSE':
            axs[2].hist(dataframe[col], color='gray')
            axs[2].set_title('Third Course')
    fig.savefig(os.path.join(BASE_PATH, 'Results', 'Part1', 'Distribution cost per course.jpg'))
    plt.close('all')
     #saubvegarder la figure dans results,part1 avec le titre distribuion-plot


def plotting_bar(dataframe: object):  # fonction de plotting du dataframe resultat qu'on a eu apres avoir lu le fichier csv

    plt.close('all')
    x = np.mean(dataframe['FIRST_COURSE']) #  #utiliser la bibliotheque numpy calculer la moyenne
    y = np.mean(dataframe['SECOND_COURSE'])
    w = np.mean(dataframe['THIRD_COURSE'])
# Create bars
    height = [x, y, w]
    bars = ('First Course', 'Second Course', 'Third Course')
    x_pos = np.arange(len(bars))
#Bars color
    plt.bar(x_pos, height, color=['blue', 'purple', 'green'], edgecolor='black')
    plt.xticks(x_pos, bars)

    plt.savefig(os.path.join(BASE_PATH, 'Results', 'Part1', 'Barplot cost per course.jpg')) #saubvegarder la figure dans results,part1 avec le titre distribuion-plot


class Prices:   #plsrs fonctions qui ont le meme objectif

    def price_1course(self, price: float) -> float:
        if price < 3:
            food = 0    #food=0 on fait ca pour créer une colonne
            drink = price     #drink=0
        elif price > 3 and \
                price < 15:
            food = 3
            drink = price - 3
        elif price > 15 and \
                price < 20:
            food = 15
            drink = price-15
        else:
            food = 20
            drink = price-20
        return food, drink

    def price_2course(self, price: float) -> float:
        if price < 9:
            food = 0
            drink = price
        elif price > (9 and \
                price < 20):
            food = 9
            drink = price - 9
        elif (20 < price and \
              price < 25):
            food = 20
            drink = price - 20
        elif (price > 25 and \
                price < 40):
            food = 25
            drink = price - 25
        else:
            food = 40
            drink = price - 40
        return food, drink

    def price_3course(self, price: float) -> float:
        if price < 10:
            food = 0
            drink = price
        elif (price > 10 and \
                price < 15):
            food = 10
            drink = price - 10
        else:
            food = 15
            drink = price - 15
        return food, drink


if __name__ == '__main__':

    path = os.path.join(BASE_PATH, 'Data', 'part1.csv') #endroit du part1
    dataframe = reader.read_csv(path) #lire le fichier csv
    plotting_hist(dataframe) #afficher le plot
    plotting_bar(dataframe)
    prices_obj = Prices() #appelle la classe une fois pour  ne pas repeter
    dataframe['course_1_food'] = dataframe['FIRST_COURSE'].apply(lambda x: prices_obj.price_1course(x)[0]) #j'applique la fonction dans chaque ligne de la colonne
    dataframe['course_1_drink'] = dataframe['FIRST_COURSE'].apply(lambda x: prices_obj.price_1course(x)[1])
    dataframe['course_2_food'] = dataframe['SECOND_COURSE'].apply(lambda x: prices_obj.price_2course(x)[0])
    dataframe['course_2_drink'] = dataframe['SECOND_COURSE'].apply(lambda x: prices_obj.price_2course(x)[1])
    dataframe['course_3_food'] = dataframe['THIRD_COURSE'].apply(lambda x: prices_obj.price_3course(x)[0])
    dataframe['course_3_drink'] = dataframe['THIRD_COURSE'].apply(lambda x: prices_obj.price_3course(x)[1])
    dataframe.to_excel(os.path.join(BASE_PATH, 'Results', 'Part1', 'Cost of drinks.xlsx')) #sauvegarder la figure dans results,part1 avec le titre distribuion-plot
