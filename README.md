**Restaurant Simulation Project

The objective of this project is simulate a restaurant, generating distribution of the potential clients and their costumer profiles.
---

## Part 1 - Exploratory Data Analysis (part_1.py)

In this part we generated a histogram of the courses bought by the clients and also the price of the drinks bought in each course.

---

## Part 2 - Clustering Data (part_2.py)

Using the k-means model we generated 4 clusters and then we attributed labels following each customer profile.


---
## Part 3 - Determining Distributions (part_3.py)

In this part of the code we studied the distributions of the data with the clusters already included in our dataframes.
We needed to merge (in order to compare with the real results) and aggregate this data with the objective of have all the 
distributions we needed.

---

## Part 4 - Simulation (part_4.py and checking_simulations.py)

In this part we simulated the restaurant receiving random customers (following the probability of each one, that was established). 
In order to organize these results we built a database in sqlite and for calculate the final questions of the project we created some 
queries in this database, modifying the data and getting the results we needed.
