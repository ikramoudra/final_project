import pandas as pd
import os

BASE_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))


def read_csv(path: str) -> object:
    dataframe = pd.read_csv(path, sep=',')
    return dataframe
