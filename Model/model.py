from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import numpy as np
from Reader import reader
import os
BASE_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__),".."))
def clustering(dataframe):

    kmeans = KMeans(n_clusters=4)

    y = kmeans.fit_predict(dataframe[['FIRST_COURSE', 'SECOND_COURSE', 'THIRD_COURSE']])

    dataframe['Cluster'] = y

    return dataframe


path = os.path.join(BASE_PATH, 'Data', 'part1.csv') #endroit du part1
dataframe = reader.read_csv(path) #lire le fichier csv
