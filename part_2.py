from Model import model
from Reader import reader
import os
import matplotlib.pyplot as plt
BASE_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__)))
path = os.path.join(BASE_PATH, 'Data', 'part1.csv') #endroit du part1
dataframe = reader.read_csv(path) #lire le fichier csv
y = model.clustering(dataframe) #Call the function we already create at Model

def labels(number_cluster):

    if number_cluster == 0:
        label = 'Fitness customer'
    elif number_cluster == 1:
        label = 'Business customer'
    elif number_cluster == 2:
        label = 'Other customers'
    else:
        label = 'Retired customer'
    return label


y['label'] = dataframe['Cluster'].apply(lambda x: labels(x))

fig = plt.figure()
ax = plt.axes(projection='3d')

ax.scatter(y[y['Cluster']==0]['FIRST_COURSE'], y[y['Cluster']==0]['SECOND_COURSE'], y[y['Cluster']==0]['THIRD_COURSE'], color='red', label = 'Cluster 0')
ax.scatter(y[y['Cluster']==1]['FIRST_COURSE'], y[y['Cluster']==1]['SECOND_COURSE'], y[y['Cluster']==1]['THIRD_COURSE'], color='green', label = 'Cluster 1')
ax.scatter(y[y['Cluster']==2]['FIRST_COURSE'], y[y['Cluster']==2]['SECOND_COURSE'], y[y['Cluster']==2]['THIRD_COURSE'], color='yellow', label = 'Cluster 2')
ax.scatter(y[y['Cluster']==3]['FIRST_COURSE'], y[y['Cluster']==3]['SECOND_COURSE'], y[y['Cluster']==3]['THIRD_COURSE'], color='blue', label = 'Cluster 3')
ax.set_xlabel('FIRST_COURSE')
ax.set_ylabel('SECOND_COURSE')
ax.set_zlabel('THIRD_COURSE')
ax.legend()

fig.savefig(os.path.join(BASE_PATH, 'Results', 'Part2', 'Clustering.jpg'))

y.to_excel(os.path.join(BASE_PATH, 'Results', 'Part2', 'Type of customers.xlsx'))
