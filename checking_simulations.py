import pandas as pd
import os
BASE_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__)))
import warnings
warnings.filterwarnings('ignore')
import sqlite3


def what_if_healthy_doubled():


    query_healthy_double = \
        """
        WITH  healthy as (
        SELECT * FROM simulation
        WHERE CUSTOMERTYPE='Healthy'
        ) ,
        all_data as ( 
        SELECT * FROM simulation)
        SELECT * FROM healthy 
        UNION ALL
        SELECT * FROM all_data
        """
    db_file = os.path.join(BASE_PATH, 'db', 'database.db')

    conn = sqlite3.connect(db_file)

    all_data = pd.read_sql_query("SELECT * FROM simulation", conn)

    all_data['TOTAL'] = all_data[['TOTAL1', 'TOTAL2', 'TOTAL3']].sum(axis=1)

    sum_all_revenue = all_data['TOTAL'].sum()

    healthy_doubled = pd.read_sql_query(query_healthy_double, conn)

    healthy_doubled['TOTAL'] = healthy_doubled[['TOTAL1', 'TOTAL2', 'TOTAL3']].sum(axis=1)

    healthy_doubled_revenue = healthy_doubled['TOTAL'].sum()

    return healthy_doubled_revenue - sum_all_revenue


def what_if_price_spaghetti_doubled():
    query_spaghetti_double = \
        """
        SELECT *, 
        CASE WHEN COURSE2='Spaghetti' THEN
            TOTAL2 + 20
        ELSE TOTAL2
        END TOTAL_W_Spaghetti_Doubled
        FROM simulation

        """
    db_file = os.path.join(BASE_PATH, 'db', 'database.db')
    conn = sqlite3.connect(db_file)

    all_data = pd.read_sql_query("SELECT * FROM simulation", conn)

    all_data['TOTAL'] = all_data[['TOTAL1', 'TOTAL2', 'TOTAL3']].sum(axis=1)

    sum_all_revenue = all_data['TOTAL'].sum()

    spaghetti_doubled = pd.read_sql_query(query_spaghetti_double, conn)

    spaghetti_doubled['TOTAL'] = spaghetti_doubled[['TOTAL1', 'TOTAL_W_Spaghetti_Doubled', 'TOTAL3']].sum(axis=1)

    spaghetti_doubled_revenue = spaghetti_doubled['TOTAL'].sum()

    return spaghetti_doubled_revenue - sum_all_revenue

if __name__ == '__main__':

    healthy_doubled_diff = what_if_healthy_doubled()

    spaghetti_doubled_diff = what_if_price_spaghetti_doubled()
